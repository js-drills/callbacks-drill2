let fs = require("fs");
const path = require("path");

let listpath = path.join(__dirname, "./lists_1.json");
let cardspath = path.join(__dirname, "./cards_1.json");
let boardspath = path.join(__dirname, "./boards_1.json");
let problem1 = require("./callback1.cjs");
let problem2 = require("./callback2.cjs");
let problem3 = require("./callback3.cjs");

const problem6function = (callbackFunction) => {
  setTimeout(() => {
    // finding board details of Thanos
    try {
      fs.readFile(boardspath, "utf-8", (boardserror, boardsdata) => {
        if (boardserror) {
          return callbackFunction(boardserror);
        }
        try {
          let thanosId = JSON.parse(boardsdata).reduce(
            (accumlator, objects) => {
              if (objects.name === "Thanos") {
                accumlator = objects.id;
              }
              return accumlator;
            },
            ""
          );
          if (thanosId) {
            problem1(thanosId, (error, board) => {
              if (error) {
                console.log(error);
                return;
              }
              console.log(
                `The board details of thanos are: ${JSON.stringify(board)}\n`
              );
            });

            // finding the thanos list using thanos Id as board Id
            problem2(thanosId, (error, thanoslistsdetails) => {
              if (error) {
                console.log(error);
                return;
              }
              console.log(
                `The list details of thanos are :\n ${JSON.stringify(
                  thanoslistsdetails
                )}\n`
              );
            });
          }
        } catch (error) {
          console.log(error);
        }
      });

      fs.readFile(listpath, "utf-8", (listerror, listdata) => {
        if (listerror) {
          return callbackFunction(listerror);
        }
        try {
          let listIds = Object.values(JSON.parse(listdata)).reduce(
            (accumlator, currentarray) => {
              currentarray.forEach((element) => {
                accumlator.push(element.id);
              });
              return accumlator;
            },
            []
          );
          const names = ["Mind", "Space", "Soul", "Time", "Power",'Reality'];
          let i = 0;
          if (Array.isArray(listIds)) {
            listIds.forEach((listId) => {
              if (listId) {
                problem3(listId, (error, cardsdata) => {
                  if (error) {
                    console.log(error);
                    return;
                  }
                  console.log(
                    `The List of Cards by ${
                      names[i++]
                    } name are: \n ${JSON.stringify(cardsdata)}\n\n`
                  );
                });
              }
            });
          }
        } catch (error) {}
      });
    } catch (error) {
      console.log(error);
    }
  }, 5000);
};

module.exports = problem6function;
