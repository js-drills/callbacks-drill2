let fs = require("fs");
const path = require("path");

let filepath = path.join(__dirname, "./cards_1.json");

let findingCards = (listId, callbackFunction) => {
    fs.readFile(filepath, 'utf-8', (error, data) => {
        if (error) {
            callbackFunction(error)
        }
        setTimeout(() => {
            try {
                if (JSON.parse(data).hasOwnProperty(listId)) {
                  let cards_data = JSON.parse(data)[listId];
                  callbackFunction(null, cards_data);
                }
            } catch (error) {
                callbackFunction(error)
            }
        },4000)
    })
}

module.exports= findingCards