let fs = require('fs')

const path = require("path");

let filepath = path.join(__dirname, "./boards_1.json");
const getBoardInfo = (boardId, callbackFunction) => {
    fs.readFile(filepath, 'utf-8', (error, boardData) => {
        if (error) {
            callbackFunction(error)
            return
        }
        setTimeout(() => {
            try {
              const boards = JSON.parse(boardData);
              let board = boards.find((board) => board.id === boardId);
              if (board) {
                callbackFunction(null, board);
              } else {
                callbackFunction(`not find any key with board Id :${boardId}`);
              }
            } catch (error) {
              callbackFunction(error);
            }
        },2000)
    })
}

module.exports = getBoardInfo