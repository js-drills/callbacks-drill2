
let fs = require('fs')
const path  = require("path");

let filepath = path.join(__dirname, "./lists_1.json");

const findingList = (boardId, callbackFunction) => {
    
        fs.readFile(filepath, 'utf-8', (error, data) => {
            if (error) {
                callbackFunction(error)
                return
            }
            setTimeout(() => {
                try {
                  const jsonData = JSON.parse(data);
                  if (jsonData.hasOwnProperty(boardId)) {
                    let listsDataset = jsonData[boardId];
                    callbackFunction(null, listsDataset);
                  } else {
                    callbackFunction(null, "BoardId not found");
                  }
                } catch (error) {
                  callbackFunction(error);
                }
            },3000)
        })
     
}
 
module.exports = findingList